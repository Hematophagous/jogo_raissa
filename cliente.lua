function novo_cliente()
	require 'pedido'

	local cliente = {
		sprite, 
		anim = {},
		actAnim,
		pedido = {},
		pediu, 
		paciencia,
		paciencia_comida,
		vai_embora,
		atendido,
		sentado,
		posicao = {x, y},
		iniciado = nil, 
		mesa,
		tempo_comer
	}

	function cliente:init(paciencia, posicao, tempo_comer)
		self.sprite = love.graphics.newImage('cliente_'..math.random(1, 1)..'.png')
		self.grid = anim8.newGrid(64, 64, self.sprite:getWidth(), self.sprite:getHeight())
		self.anim = {
			down = anim8.newAnimation(self.grid('1-9',3), 0.5),
			right = anim8.newAnimation(self.grid('1-9',4), 0.5),
		}
		self.tempo_comer = tempo_comer
		self.actAnim = self.anim.down
		self.t = paciencia
		self.paciencia = paciencia
		self.paciencia_comida = paciencia
		self.vai_embora = false
		self.sentado = false
		self.pediu = false
		self.atendido = false
		self.posicao.x, self.posicao.y = posicao.x, posicao.y
		self.iniciado = true
		self.mesa = nil
	end

	function cliente:update(dt, mesa, pedidos_nivel)
		if not self.vai_embora then
			if not self.sentado and mesa.i >= 0 then
				if not mesa.ocupada then
					if self.mesa == nil and mesa.cliente == nil then
						self.mesa = mesa
						mesa.cliente = self
					elseif self.mesa == mesa then
						self:andar_ate_mesa(dt, mesa)
					end
				end
			elseif not self.sentado and mesa.i < 0 then
				self.paciencia = self.paciencia - dt

				if self.paciencia < 0 then
					self.vai_embora = true
				end
			elseif self.sentado and not self.pediu then
				self.pediu = true
				self.pedido = self.fazer_pedido(pedidos_nivel)
			elseif self.pediu and not self.atendido then
				self.paciencia_comida = self.paciencia_comida - dt

				if self.paciencia_comida < 0 then
					self.vai_embora = true
					self.mesa.cliente = nil
					self.mesa.ocupada = false
				end
			elseif not self.vai_embora then
				self:comer(dt)
			else
				self.mesa.cliente = nil
				self.mesa.ocupada = false
				self.vai_embora = true
			end
		end
		self.actAnim:update(dt)
	end

	function cliente:comer(dt)
		self.tempo_comer = self.tempo_comer - dt

		if self.tempo_comer then 
			self.vai_embora = true
		end
	end

	function cliente:ser_atendido()
		--if mesa == mesa then
			self.atendido = true
		--end
	end

	function cliente.fazer_pedido(pedidos_nivel)
		local n = math.random(pedidos_nivel)

		k =  getpedido(n)
		return k
	end

	function cliente:andar_ate_mesa(dt, mesa)
		if mesa.cliente == self then
			if self.posicao.x < mesa.x then
				self.posicao.x = self.posicao.x + 100*dt
				self.actAnim = self.anim.right
			elseif self.posicao.y < mesa.y then
				self.posicao.y = self.posicao.y + 100*dt
				self.actAnim = self.anim.down
			else
				self.posicao.x, self.posicao.y = mesa.x, mesa.y
				self.sentado = true
				mesa.ocupada = true
				self.actAnim = self.anim.down
			end
		end
	end

	function cliente:draw()
		love.graphics.setColor(1, 0, 0)
		for i = 1, cliente.paciencia do
			love.graphics.rectangle("fill", (self.posicao.x - self.paciencia/2) + 5*i, self.posicao.y - 50, 5, 5)
		end
		love.graphics.setColor(0, 1, 0)
		for i = 1, cliente.paciencia_comida do
			love.graphics.rectangle("fill", (self.posicao.x - self.paciencia/2) + 5*i, self.posicao.y - 42, 5, 5)
		end
		love.graphics.setColor(1, 1, 1)
		self.actAnim:draw(self.sprite, self.posicao.x, self.posicao.y - 48)
		
		if self.pedido.id ~= nil then
			love.graphics.draw(self.pedido.img, self.posicao.x+16, self.posicao.y-48)
		end
		
	end

	return cliente
end