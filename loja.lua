
local loja = {
	itens = {
		{nome = 'mesa', preco = 100},
		{nome = 'garcon', preco = 200},
	},
	dinheiro,
	num_mesas = 4,
	num_garcons = 1,
	font,
	ativa,
	logo = love.graphics.newImage("logo.png")
}

local seta = love.graphics.newImage("seta.png")
local mesa = love.graphics.newImage("mesa.png")
local garcon = love.graphics.newImage("garcons_1.png")

local grid = anim8.newGrid(48, 48, garcon:getWidth(), garcon:getHeight())
local anim = anim8.newAnimation(grid('1-3',4, '1-3',  3, '1-3', 1, '1-3', 2), .2)

function loja:init(dinheiro)
	self.dinheiro = dinheiro - 600
	self.font = love.graphics.newFont( 20, "normal")
	self.font2 = love.graphics.newFont( 40, "normal")
	self.ativa = true
end
	
function loja:draw()
	love.graphics.setFont(self.font)
	mx, my = love.mouse.getPosition()
	width = love.graphics.getWidth()
	height = love.graphics.getHeight()


	

	love.graphics.setColor(1, 1, 1)
	love.graphics.draw(self.logo, love.graphics:getWidth()/2-150, 0, 0, 2, 2)
	love.graphics.setColor(1, 0.7, 0)
	love.graphics.print("O JOGO", love.graphics:getWidth()/2+40, self.logo:getHeight()*1.5)
	love.graphics.setColor(1, 1, 1)
	love.graphics.rectangle("fill", 100, 25, 100, 100)
	love.graphics.rectangle("fill", 100, 135, 140, 40)
	love.graphics.rectangle("fill", 210, 25, 30, 45)
	love.graphics.rectangle("fill", 210, 80, 30, 45)
	love.graphics.draw(mesa, 125, 50)
	love.graphics.draw(seta, 210, 25)
	love.graphics.draw(seta, 210, 125, 0, 1, -1)
	love.graphics.setColor(0, 0, 0)
	love.graphics.print("Mesas: "..self.num_mesas, 100, 147)


	love.graphics.setColor(1, 1, 1)
	love.graphics.rectangle("fill", 100, 225, 100, 100)
	love.graphics.rectangle("fill", 100, 335, 140, 40)
	love.graphics.rectangle("fill", 210, 225, 30, 45)
	love.graphics.rectangle("fill", 210, 280, 30, 45)
	anim:update(love.timer.getDelta())
	anim:draw(garcon, 125, 250)
	love.graphics.draw(seta, 210, 225)
	love.graphics.draw(seta, 210, 325, 0, 1, -1)
	love.graphics.setColor(0, 0, 0)
	love.graphics.print("Garçons: "..self.num_garcons, 100, 347)

	love.graphics.setColor(0, 1, 0)
	love.graphics.print("$ "..self.dinheiro, 100, 400)
	love.graphics.setColor(1, 1, 1)
	love.graphics.print("Garçon $"..self.itens[2].preco, 100, 430)
	love.graphics.print("Mesa $"..self.itens[1].preco, 100, 460)

	love.graphics.setFont(self.font2)
	love.graphics.setColor(.2, 1, .5)
	love.graphics.rectangle("fill", 300, 400, 250, 70)
	love.graphics.setColor(0, 0, 0)
	love.graphics.print("Jogar", 370, 410)	

end

function loja:mousepressed(x, y, button)
	if x > 210 and x < 240 then
		if y > 25 and y < 70 then
			if self.dinheiro >= self.itens[1].preco then
				self.dinheiro = self.dinheiro - self.itens[1].preco
				self.num_mesas = self.num_mesas + 1
			end 
		elseif y > 80 and y < 125 then
			if self.num_mesas > 4 then
				self.num_mesas = self.num_mesas - 1
				self.dinheiro = self.dinheiro + self.itens[1].preco
			end
		end

		if y > 225 and y < 270 then
			if self.dinheiro >= self.itens[2].preco then
				self.dinheiro = self.dinheiro - self.itens[2].preco
				self.num_garcons = self.num_garcons + 1
			end 
		elseif y > 280 and y < 325 then
			if self.num_garcons > 1 then
				self.num_garcons = self.num_garcons - 1
				self.dinheiro = self.dinheiro + self.itens[2].preco
			end
		end
	elseif x > 300 and x < 550 then
		if y > 400 and y < 470 then
			loja.ativa = false
			return self.dinheiro, self.num_garcons, self.num_mesas
		end
	end
	return nil
end

return loja
