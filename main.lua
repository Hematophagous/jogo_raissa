local nivel = {
	require 'nivel_1',
	--require 'nivel_2',
	--require 'nivel_3',
}

anim8 = require 'anim8'

local loja = require 'loja'

local dinheiro = 1000
local mesa
local garcons

local i = 1
local num_niveis = 0

function love.load()
	bkg = love.graphics.newImage("fundo2.png")
	love.graphics.setDefaultFilter("nearest")
	
	loja:init(1000)
end

function love.update(dt)
	if not game_over then
		if not loja.ativa then
			if #nivel > 0 then
				if nivel[i].correndo then
					nivel[i]:update(dt)
				else
					dinheiro = nivel[i]:encerrar()
					table.remove(nivel, i)
					i = i + 1
					if i <= num_niveis then
						nivel[i]:carregar()
					else
						i = i - 1
					end
				end
			else
				if dinheiro <= 1000 then
					game_over = true
				end
			end
		end
	end
end

function love.draw()
	if not loja.ativa then
		if #nivel > 0 then
			love.graphics.draw(bkg, 0, 0)
			if nivel[i].correndo then
				nivel[i]:draw()
			end
		end
	else
		loja:draw()
	end
end

function love.mousereleased(x, y, button)
	
	if loja.ativa then
		dinheiro, garcons, mesas = loja:mousepressed(x, y, button)
		if mesas ~= nil then
			nivel[i]:carregar(dinheiro, garcons, mesas)
		end
	elseif #nivel > 0 then
		nivel[i]:getBounds(x, y, button)
	end


end

