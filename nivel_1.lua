require 'cliente'
require 'garcon'

local nivel_1 = {
	correndo,
	removeu = false,
	dia = 0,
	intervalo = 2,

	clientes = {},

	garcons = {},

	t = 1,
	dinheiro = 0,

	fila = {}, 

	pedidos = {
		{nome = "Burgão", preco = 10, tempo = 10, img = love.graphics.newImage("1.png")},
		{nome = "Saladao", preco = 2, tempo = 5, img = love.graphics.newImage("1.png")},
	},

	mesas,
}

function inserir_cliente(clientes)
	local paciencia = 20
	local posicao = {
		x = 10,
		y = 20,
	}

	table.insert(clientes, novo_cliente())

	for i, cliente in pairs(clientes) do
		if cliente.iniciado == nil then
			cliente:init(paciencia, posicao, 10)
		end
	end
	
end

function inserir_garcon(garcons, num_garcons)
	local velocidade = 75

	for i=1,num_garcons do
		table.insert(garcons, novo_garcon())
	end

	for i, garcon in pairs(garcons) do
		if garcon.iniciado == nil then
			garcon:init(velocidade, 64*i)
		end
	end
	
end

function iniciar_mesas(num_mesas)
	local mesas = {}
	local interval = (love.graphics.getWidth() - love.graphics.getWidth()/2)/num_mesas
	local k, l = interval, interval
	local break_ = false

	for j=1, num_mesas do
		
		if j > num_mesas/2 and not break_ then
			k = interval - (j-1)*64
			l = l + interval
			break_ = true
		end

		mesas[j] = {
			img = love.graphics.newImage("mesa.png"),
			ocupada = false,
			i = j,
			x = k + 65*(j-1),
			y = 128 + l, --+ 50*(j-1),
			cliente = nil,
		}	
		k = k + interval
	end

	return mesas
end

function nivel_1:carregar(dinheiro, num_garcons, num_mesas)
	self.correndo = true
	self.mesas = iniciar_mesas(num_mesas)
	self.dinheiro = dinheiro
	inserir_garcon(self.garcons, num_garcons)
end

function nivel_1:update(dt)

	self.t = self.t - dt
	self.intervalo = self.intervalo - dt
	self.dia = self.dia + 1.16*dt

	if self.dia < 50 then
		
		for i, peido in pairs(self.fila) do
			if not self.removeu then
				peido.tempo = peido.tempo - dt
				if peido.tempo < 0 then
					for j, garcon in pairs(self.garcons) do					
						if #garcon.bandeja == 0 then				
							table.insert(garcon.bandeja, peido)
							table.remove(self.fila, i)
							removeu = true
							break
						elseif #garcon.bandeja < 4 then
							table.insert(garcon.bandeja, peido)
							table.remove(self.fila, i)
							removeu = true
							break
						end
					end
				end
			else
				break
			end
		end

		if self.intervalo < 0 then
			if math.random(0, 10) > 5 then
				inserir_cliente(self.clientes)
			end
			self.intervalo = 2
		end

		local mesa = {i = -1, x = -1, j = -1}
	
		for i, cliente in pairs(self.clientes) do
			if cliente.vai_embora then
				if cliente.atendido then
					self.dinheiro = self.dinheiro + self.pedidos[cliente.pedido.id].preco
				end
				table.remove(self.clientes, i)
				for i, desk in pairs(self.mesas) do
					if desk.cliente == cliente then
						desk.cliente = nil
						desk.ocupada = false
					end	
				end
			else
				for i, table in pairs(self.mesas) do
					if table.cliente == nil or table.cliente == cliente then
						mesa = table
					end	
				end
				cliente:update(dt, mesa, #self.pedidos)
			end
		end
	else 
		self.intervalo = 5
		if #self.clientes == 0 then
			self.correndo = false
		end
		for i, cliente in pairs(self.clientes) do
			if cliente.vai_embora then
				if cliente.atendido then
					self.dinheiro = self.dinheiro + self.pedidos[cliente.pedido.id].preco
				end
				table.remove(self.clientes, i)
			elseif not cliente.sentado then
				table.remove(self.clientes, i)
			else
				cliente:update(dt, {i = -1, x = -1, y = -1}, #self.pedidos)
			end
		end
	end
	for i, garcon in pairs(self.garcons) do
			garcon:update(dt, mesa)
	end
end

function nivel_1:draw()
	
	for i, cliente in pairs(self.clientes) do
		cliente:draw()
	end

	for i, garcon in pairs(self.garcons) do
		garcon:draw()
	end

	for i, mesa in pairs(self.mesas) do
		love.graphics.draw(mesa.img, mesa.x, mesa.y)
	end

	for i, garcon in pairs(self.garcons) do
		if garcon.verBandeja then
			garcon:draw_bandeja()
		end
	end
	self:gui()

end

function nivel_1:encerrar()
	for i, mesa in pairs(self.mesas) do
		table.remove(self.clientes, i)
	end
	return self.dinheiro
end

function nivel_1:gui()
	love.graphics.setColor(1, 1, 1)
	love.graphics.rectangle("fill", love.graphics:getWidth() - 100, 0, 100, 40)
	love.graphics.setColor(0, 0, 0)
	love.graphics.setLineWidth(5)
	love.graphics.rectangle("line", love.graphics:getWidth() - 100, 0, 100, 40)
	love.graphics.setColor(0, 1, 0)
	love.graphics.print("$"..self.dinheiro, love.graphics:getWidth() - 80, 10, 0, .5, .5)
	love.graphics.setLineWidth(1)
	love.graphics.setColor(1, 1, 1)

end

function nivel_1:getBounds(x, y, button)
	for i, mesa in pairs(self.mesas) do
		
		if x > mesa.x and x < mesa.x + 50 then
			if y > mesa.y and y < mesa.y + 50 then
				if mesa.cliente ~= nil then
					table.insert(self.fila, mesa.cliente.pedido)
				end
			end
		end
	end

	for i, garcon in pairs(self.garcons) do
		garcon:mousepressed(x, y, button)
		if garcon.selecionado ~= nil then
			for i, mesa in pairs(self.mesas) do
				if x > mesa.x and x < mesa.x + 50 then
					if y > mesa.y and y < mesa.y + 50 then
						table.insert(garcon.destinos, mesa)
					end
				end
			end
		end
	end
end

return nivel_1