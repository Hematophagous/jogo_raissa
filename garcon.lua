function novo_garcon()

	local garcon = {
		sprite,
		destinos,
		bandeja,
		posicao = {x, y},
		iniciado = nil, 
		velocidade = 100,
		balcao,
		imgBandeja,
		verBandeja,
	}

	function garcon:init(velocidade, yk)
		self.sprite = love.graphics.newImage("garcons_"..math.random(1, 3)..".png")
		self.grid = anim8.newGrid(48, 48, self.sprite:getWidth(), self.sprite:getHeight())

		self.anim = {
			up = anim8.newAnimation(self.grid('1-3',4), .2),
			left = anim8.newAnimation(self.grid('1-3',2), .2),
			down = anim8.newAnimation(self.grid('1-3',1), .2),
			right = anim8.newAnimation(self.grid('1-3',3), .2),
		}
		self.actAnim = self.anim.left
		self.balcao = {x = 780, y = 580-yk} 
		self.posicao = {x = 780, y = 580-yk} 
		self.destinos = {}
		self.bandeja = {}
		self.iniciado = true
		self.velocidade = velocidade
		self.verBandeja = false
		self.imgBandeja = love.graphics.newImage("bandeja.png")
		self.selecionado = nil
	end

	function garcon:update(dt)
		if #self.destinos > 0 then
			self:andar_ate_mesa(dt, self.destinos[1])
		else
			self:aguardar(dt)
		end
		
		self.actAnim:update(dt)
	end

	function garcon:andar_ate_mesa(dt, mesa)
		if self.posicao.x > mesa.x then
			self.posicao.x = self.posicao.x - self.velocidade*dt
			self.actAnim = self.anim.left
		elseif self.posicao.y > mesa.y then
			self.posicao.y = self.posicao.y - self.velocidade*dt
			self.actAnim = self.anim.up
		else
			for i, pedido in pairs(self.bandeja) do
				if mesa.cliente ~= nil then
					if pedido.id == mesa.cliente.pedido.id then
						mesa.cliente:ser_atendido()
						table.remove(self.bandeja, i)
						self.selecionado = nil
						return true
					end
				else
					table.remove(self.destinos, 1)			
					self.selecionado = nil
					return false		
				end
			end
			table.remove(self.destinos, 1)
			self.selecionado = nil			
			return false
		end
	end

	function garcon:aguardar(dt)
		if self.posicao.x < self.balcao.x then
			self.posicao.x = self.posicao.x + self.velocidade*dt
			self.actAnim = self.anim.right
		elseif self.posicao.y < self.balcao.y then
			self.posicao.y = self.posicao.y + self.velocidade*dt
			self.actAnim = self.anim.down
		end
	end

	

	function garcon:draw()
		self.actAnim:draw(self.sprite, self.posicao.x-32, self.posicao.y - 48)
		love.graphics.setColor(1, 1, 1)
	end

	function garcon:mousepressed(x, y, button)
		if not self.verBandeja then
			if x > self.posicao.x-48 and x < self.posicao.x + 16 then
				if y > self.posicao.y-48 and y < self.posicao.y + 16 then
					self.verBandeja = true
				end
			end
		else
			if selecionado == nil then
				local spots = {
					{x = love.graphics:getWidth()/2 - 80, y = love.graphics:getHeight()/2 - 80},
					{x = love.graphics:getWidth()/2 - 80, y = love.graphics:getHeight()/2 + 48},
					{x = love.graphics:getWidth()/2 + 48, y = love.graphics:getHeight()/2 - 80},
					{x = love.graphics:getWidth()/2 + 48, y = love.graphics:getHeight()/2 + 48},
				}
				
				for i,item in pairs(self.bandeja) do
					if x > spots[i].x and x < spots[i].x + 32 then
						if y > spots[i].y and y < spots[i].y + 32 then
							self.selecionado = item
						end	
					end	
				end
			end
			
			self.verBandeja = false

		end
	end

	function garcon:draw_bandeja()
		local centerx, centery = love.graphics:getWidth()/2 - self.imgBandeja:getWidth()/4, love.graphics:getHeight()/2 - self.imgBandeja:getHeight()/4
		local spots = {
			{x = centerx, 		y = centery},
			{x = centerx + 128, y = centery},
			{x = centerx, 		y = centery + 128},
			{x = centerx + 128, y = centery + 128},
		}
		
		love.graphics.draw(self.imgBandeja, love.graphics:getWidth()/2 - self.imgBandeja:getWidth()/2, love.graphics:getHeight()/2 - self.imgBandeja:getHeight()/2)
		for i,item in pairs(self.bandeja) do
			love.graphics.draw(item.img, spots[i].x,spots[i].y, 0, 2, 2, item.img:getWidth()/2, item.img:getHeight()/2)
			love.graphics.setColor(1, 1, 1)
		end
	end

	return garcon
end